package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "Hello, World by mike")
	})

	err := r.Run()
	if err != nil {
		fmt.Println("error", err)
	}
}
